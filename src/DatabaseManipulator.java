import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DatabaseManipulator {

    MySQLConnector myConnection = new MySQLConnector();
   // ArrayList myArrayList =
   // new ArrayList();
    Scanner input = new Scanner(System.in);
    HashMap<String, DataUser> datauser = new HashMap<>();

    void cetakData() {
        ambilData();
        for (String x : datauser.keySet()) {
            System.out.print(x + " ");
            DataUser temp = datauser.get(x);
            System.out.print(temp.getNama() + "\t");
            System.out.print(temp.getJenis_kelamin() + "\t");
            System.out.println(temp.getNo_telp());
        }
    }

        void ambilData () {
            try {
                datauser.clear();
                Connection conn = myConnection.connectionOpen();
                String sqlQuery = "SELECT * FROM `tb_siswa`";
                Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(sqlQuery);

                while (resultSet.next()) {
                    datauser.put(resultSet.getString("id"), new DataUser(
                            resultSet.getString("nama"),
                            resultSet.getString("jenis_kelamin"),
                            resultSet.getString("no_telp")));

                /*myArrayList.add(resultSet.getString("id"));
                myArrayList.add(resultSet.getString("nama"));
                myArrayList.add(resultSet.getString("jenis_kelamin"));
                myArrayList.add(resultSet.getString("no_telp"));  */
                }
            } catch (Exception error) {
                System.out.println("Ambil Data : " + error.getMessage());
            } finally {
                myConnection.connectionClose();
            }
        }

        void insertData () {
            try {
                Connection conn = myConnection.connectionOpen();
                System.out.print("Masukkan Nama : ");
                String nama = input.nextLine();
                System.out.print("Masukkan Jenis Kelamin : ");
                String jenis_kelamin = input.nextLine();
                System.out.print("Masukkan No Telp : ");
                String no_telp = input.nextLine();
                String sqlQuery = "INSERT INTO `tb_siswa` (`id`, `nama`, `jenis_kelamin`, `no_telp`) VALUES (NULL, '" + nama + "', '" + jenis_kelamin + "', '" + no_telp + "')";
                Statement statement = conn.createStatement();
                statement.executeUpdate(sqlQuery);
                System.out.println("Berhasil Memasukkan Data");
            } catch (Exception error) {
                System.out.println("Insert Data :" + error.getMessage());
            } finally {
                myConnection.connectionClose();
            }
        }

        void updateData () {
        ambilData();
            try {
                Connection conn = myConnection.connectionOpen();
                System.out.print("Masukkan ID yang ingin diubah : ");
                String id = input.nextLine();
                //tambahkan code untuk check ID apakah sudah ada di DATABASE
                for (String id_matcher : datauser.keySet()) {
                    if (id_matcher != id) {
                        throw new Exception("ID " + id + " belum ada di database");
                    }
                }
                System.out.print("Masukkan Nama Baru : ");
                String nama = input.nextLine();
                System.out.print("Jenis Kelamin : ");
                String jenis_kelamin = input.nextLine();
                System.out.print("No telepon : ");
                String no_telp = input.nextLine();
                String sqlQuery = "UPDATE `tb_siswa` SET `nama` = '" + nama + "', `jenis_kelamin` = '" + jenis_kelamin + "', `no_telp` = '" + no_telp + "' WHERE `tb_siswa`.`id` = " + id + " ";
                int row = conn.prepareStatement(sqlQuery).executeUpdate();
                if (row == 0) {
                    throw new Exception("Tidak ada perubahan yang terjadi di dalam database");
                }
                System.out.println("Berhasil Merubah Data ID : " + id);
            } catch (Exception error) {
                System.out.println("Insert Data :" + error.getMessage());
            } finally {
                myConnection.connectionClose();
            }
        }

        void hapusData () {
            try {
                Connection conn = myConnection.connectionOpen();
                System.out.print("Masukkan ID yang ingin dihapus : ");
                String id = input.nextLine();
                String sqlQuery = "DELETE FROM `tb_siswa` WHERE id=" + id;
                conn.prepareStatement(sqlQuery).execute();
                System.out.println("Berhasil Menghapus Data ID : " + id);
            } catch (Exception error) {
                System.out.println("Delete Data : " + error.getMessage());
            } finally {
                myConnection.connectionClose();
            }
        }


}
